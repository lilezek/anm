

package anmEjemplosUnidad1;

import auxiliar.*;

/**
 * @author MILANOP01
 */
public class EjemploCramer2 {


    public static void main(String[] args) {
        int n = 14;
        double[][][] Hn = new double[n][][];
        for (int i = 0; i < Hn.length; i++) {
            Hn[i] = Hilbert(i + 1);
        }
        double[][] b = new double[n][];
        for (int i = 0; i < b.length; i++) {
            b[i] = new double[i + 1];
            for (int j = 0; j < b[i].length; j++) {
                b[i][j] = Hn[i][j][0];
                for (int k = 1; k < b[i].length; k++) {
                    b[i][j] = b[i][j] + Hn[i][j][k];
                }
            }
            System.out.println(Matrices.toString(b[i]));
        }
        try {
            //Apartado 1.
            for (int i = 0; i < n; i++) {
                //Matrices.escribe(Hn[5]);
                System.out.println("\n \t i = " + i + "\n Solución Hn[" + i + "] u = b:");
                double[] solucion = Matrices.Cramer(Hn[i], b[i]);
                System.out.println(" u = " + Matrices.toString(solucion));
                double[] resto = Matrices.resta(Matrices.producto(Hn[i], solucion), b[i]);
                System.out.println("||Hn[" + i + "]u-b|| = " + Matrices.norma(resto));
            }
        } catch (Matrices.ErrorMatrices e) {

        }

    }


    public static double[][] Hilbert(int n) {
        double[][] Hilbert = new double[n][n];
        for (int i = 0; i < Hilbert.length; i++) {
            for (int j = 0; j < Hilbert.length; j++) {
                Hilbert[i][j] = 1. / (i + j + 1);
            }
        }
        return Hilbert;
    }


}


