
package anmEjemplosUnidad1;

import auxiliar.*;
import auxiliar.Matrices.ErrorMatrices;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author MILANOP01
 */
public class EjemploCramer1 {
    public static void main(String[] args) {

        double[][] A = {{10, 7, 8, 7}, {7, 5, 6, 5}, {8, 6, 10, 9}, {7, 5, 9, 10}};
        //prueba a quitar alg'un t'ermino

        Matrices.escribe(A);

        double[][] invA = {{25, -41, 10, -6}, {-41, 68, -17, 10},
                {10, -17, 5, -3}, {-6, 10, -3, 2}};
        double e = 0.1;
        double[][] DeltaA = {{0, 0, 0.1 * e, 0.2 * e}, {0.08 * e, 0.04 * e, 0, 0},
                {0, -0.02 * e, -0.11 * e, 0}, {-0.01 * e, -0.01 * e, 0, -0.02 * e}};
        double[] b = {32, 23, 33, 31};

        double[] bP = {32 + 0.1 * e, 23 - 0.1 * e, 33 + 0.1 * e, 31 - 0.1 * e};

        try {
            double detA = Matrices.determinanteMenores(A);
            System.out.println("Determinante de A = " + detA);
//            double detInvA = Matrices.determinanteMenores(invA);
//            System.out.println("Determinante de invA = " + detInvA);
//
            System.out.println("\n Solución Au = b:");
            double[] solucion = Matrices.Cramer(A, b);
            System.out.println(" u = " + Matrices.toString(solucion));
            double[] resto = Matrices.resta(Matrices.producto(A, solucion), b);
            System.out.println("||Au-b|| = " + Matrices.norma(resto));
            System.out.println("\n Solución Bv = b:");

            double[][] B = Matrices.suma(A, DeltaA);
            double[] solucionB = Matrices.Cramer(B, b);
            System.out.println(" v = " + Matrices.toString(solucionB));
            resto = Matrices.resta(Matrices.producto(B, solucionB), b);
            System.out.println("||Bv-b|| = " + Matrices.norma(resto));
            System.out.println("" +
                    "Una pequeña perturbación de A (B=A+Delta(A)) produce una\n"
                    + "gran perturbación en la solucion del sistema ");
//            System.out.println("||v-u|| = "+ Matrices.norma1(Matrices.resta(solucionB, solucion)));
//         
////            System.out.println("\n Solución Aw = b':");
////            double[] solucionP = Matrices.Cramer(A, bP);
////            System.out.println(" w = "+ Matrices.toString(solucionP));
////            resto=Matrices.resta(Matrices.producto(A, solucionP), bP);
////            System.out.println("||Aw-b'|| = "+ Matrices.norma1(resto));
////            System.out.println(""+
////                   "Una pequeña perturbación de b (||b'-b|| = "+Math.sqrt(4*e)+ ") produce una\n"
////                    + "gran perturbación en la solucion del sistema " );
////            System.out.println("||w-u|| = "+ Matrices.norma1(Matrices.resta(solucionP, solucion)));
////            

            //¿¿ Que pasa si e es m'as peque~no??

        } catch (Matrices.ErrorMatrices ex) {

        }

    }

}
