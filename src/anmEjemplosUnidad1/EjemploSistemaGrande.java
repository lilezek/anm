/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package anmEjemplosUnidad1;

import auxiliar.Matrices;
import auxiliar.PanelDibujo;

import java.awt.Color;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * @author MILANOP01
 */
public class EjemploSistemaGrande {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int n = 1000000;
        double[][] AT = new double[n][3];
        double D = 3.1;
        double[] b = new double[n];
        double[] nb = new double[n];
        double[] sol = new double[n];

        //Inicializar b
        for (int i = 0; i < n; i++) {
            b[i] = 3 * Math.sin((i * 2 * Math.PI) / (n - 1));
        }

        //Inicializar AT
        AT[0][0] = D;
        AT[0][1] = -1;
        AT[0][2] = -1;
        nb[0] = b[0];

        for (int i = 1; i < n; i++) {
            AT[i][0] = D + AT[i - 1][1] / AT[i - 1][0];
            AT[i][1] = -1 + AT[i - 1][2] / AT[i - 1][0];
            AT[i][2] = -1;
            nb[i] = b[i] + nb[i - 1] / AT[i - 1][0];
        }

        //Método ascendente de Gauss
        for (int i = n - 1; i >= 0; i--) {
            sol[i] = nb[i];
            for (int k = 1; (i + k < n) && (k <= 2); k++) {
                sol[i] = sol[i] - AT[i][k] * sol[i + k];
            }
            sol[i] = sol[i] / AT[i][0];
        }

        //Cálculo del resto (A.sol - b) para comprobar solución
        double[] resto = new double[n];

        resto[0] = D * sol[0] - sol[1] - sol[2] - b[0];
        resto[n - 2] = -sol[n - 3] + D * sol[n - 2] - sol[n - 1] - b[n - 2];
        resto[n - 1] = -sol[n - 2] + D * sol[n - 1] - b[n - 1];

        for (int i = 1; i < n - 2; i++) {
            resto[i] = -sol[i - 1] + D * sol[i] - sol[i + 1] - sol[i + 2] - b[i];
        }

        System.out.println("Norma del Resto = " + Matrices.norma(resto));
        //System.out.println("Matriz AT = " + Matrices.toString(AT));

        int numPuntos = 800;
        double[][] tablaGrafica = new double[numPuntos][2];

        for (int i = 0; i < numPuntos; i++) {
            tablaGrafica[i][0] = 2 * i * Math.PI / (numPuntos - 1);
            tablaGrafica[i][1] = sol[((n - 1) * i) / (numPuntos - 1)];
        }

        PanelDibujo pd = new PanelDibujo("Solucion sistema", 10, 10, 810, 600);

        pd.addWindowListener(
                new WindowAdapter() {

                    @Override
                    public void windowClosing(WindowEvent e) {
                        System.exit(0);
                    }
                });

        pd.addEjesCoordenados(true, -0.7, 1.05 * 2 * Math.PI, -35, 35, 0, 0);
        pd.addCurva(Color.blue, tablaGrafica);
        pd.repaint();


    }

}
