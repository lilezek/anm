package anmPractica3;

import auxiliar.Matrices;
import ezequiel.Matriz;
import ezequiel.MatrizReal;

/**
 * Created by lilezek on 12/04/15.
 */
public class Ejercicio3_1 {
    public static void main(String[] args) {
        try {
            Matriz A = new MatrizReal(4, 4,
                    10., 7., 8., 7.,
                    7., 5., 6., 5.,
                    8., 6., 10., 9.,
                    7., 5., 9., 10.);
            System.out.println(A.simetrica());
            System.out.println(A);
            System.out.println(A.factorLdeCholeski().producto(A.factorLdeCholeski().traspuesta()));

            A = new MatrizReal(4, 4,
                    10., 7., 8., 7.,
                    7., 5., 6., 5.,
                    8., 6., 10., 9.,
                    7., 5., 9., 0.);
            Matriz b = new MatrizReal(4,1,32., 21., 0., 12.);
            Matriz x = A.solveGaussP(b);

            System.out.println(x);
            System.out.println(A.producto(x).resta(b).norma1());

            A = new MatrizReal(3, 3,
                    9.3746, 3.0416, -2.4371,
                    3.0416, 6.1832, 1.2163,
                    -2.4371, 1.2163, 0.);
            b = new MatrizReal(3,1,9., 8., 3.93);
            x = A.solveGaussP(b);

            System.out.println(x);
            System.out.println(A.producto(x).resta(b).norma1());

        } catch (Matrices.ErrorMatrices errorMatrices) {
            errorMatrices.printStackTrace();
        }
    }
}

