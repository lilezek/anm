/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Xfiles;

/**
 * @author Antonio
 */
public class ExpedienteXNo3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        float x;
        int y;
        double z;
        // Poner el prefijo f sobre un número de coma flotante para indicar que su precisión es float.
        x = 3.5f;
        y = (int) x;
        z = (double) x;
        System.out.println("y=" + y);
        System.out.println("z=" + z);
    }
}
