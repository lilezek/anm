/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Xfiles;

/**
 * @author Antonio
 */
public class ExpedienteXNo1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*
         *  Este bucle nunca se inicia pues la condición de continuación
         *  es falsa desde el principio.
         */
        for (int i = 20; i == 0; i--) {
            System.out.println(i);
        }

        /*
         * Este código sí es correcto:
         */
        for (int i = 20; i != 0; i--) {
            System.out.println(i);
        }
    }
}
