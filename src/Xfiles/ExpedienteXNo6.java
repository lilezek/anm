/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Xfiles;

/**
 * @author Antonio
 */
public class ExpedienteXNo6 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Explica los resultados de las siguientes instrucciones.
        double[] a = {12.6, 2.3, 6.28};
        double[] c = cambiar(a);
        double[] d = nocambiar(a);

        System.out.println(c[0]);
        System.out.println(c[1]);
        System.out.println(c[2]);
        System.out.println(d[0]);

        // Puesto que en los métodos cambiar y no cambiar
        // se asigna un array a otro, no se copia su contenido, sino
        // que en realidad se está usando siempre el array a:
        System.out.println((a == c) && (a == d));
    }

    public static double[] cambiar(double[] b) {
        double[] c = new double[b.length];
        // Aquí se debería usar un algoritmo de copia:
        c = b;
        c[0] = 3455.23;
        return c;
    }

    public static double[] nocambiar(double[] b) {
        double[] d = new double[b.length];
        // Aquí se debería usar un algoritmo de copia:
        d = b;
        return d;
    }
}
