/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Xfiles;

/**
 * @author Antonio
 */
public class ExpedienteXNo4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        // Explica los resultados de las siguientes instrucciones:

        // Al ser operaciones enteras, 4/3 = 1
        System.out.println("4/3*10=" + 4 / 3 * 10);
        // Primero 4 * 10 es 40, y luego 40/3 = 13 por ser división entera
        System.out.println("4*10/3=" + 4 * 10 / 3);
        // Otra vez, la división es entera y 4/3 = 1
        System.out.println("4/3*10.=" + 4 / 3 * 10.);
        // El producto 4. * 10 da 40., y al ser decimal, 40./3 da 13.333...
        System.out.println("4.*10/3=" + 4. * 10 / 3);
    }
}
