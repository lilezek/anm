/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Xfiles;

/**
 * @author Antonio
 */
public class ExpedienteXNo5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        // Explica los resultados de las siguientes instrucciones:
        // Preparate para abortar el programa!!!!!!!!!!

        // Dados los fallos de precisión al operar, no se debe nunca exigir
        // exactitud en los cálculos de coma flotante sin esperar un error.

        // Sin embargo, 0.125 y 0.03125 son potencias de fracciones de dos, y sus cálculos
        // son exactos.
        double x = 1.;
        while (x != 0) {
            x = x - 0.03125; // prueba despues poniendo x-0,125 o x-0.03125
            System.out.println("x = " + x);
            // Con esto ya no hace falta abortar el programa.
            if (x < 0)
                break;
        }
    }
}
