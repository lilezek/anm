/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Xfiles;

/**
 * @author Antonio
 */
public class ExpedienteXNo2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*
         * No se debe usar el operador de asignación para comparar.
         * Además, es lo mismo indicar 'a' que indicar 'a == true'.
         */
        boolean a = false;
        if (a) {
            System.out.println("a es verdadera");
        } else {
            System.out.println("a es falsa");
        }
    }
}
