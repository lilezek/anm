/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Xfiles;

/**
 * @author Antonio
 */
public class ExpedienteXNo7 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Explica los resultados de las siguientes instrucciones.
        System.out.println("OJO CON LAS EVALUACIONES DE 2^N ");
        int mul;  //prueba también escribiendo long o double en lugar de int
        mul = 1;
        for (int i = 1; i < 32; i++) {
            mul = mul * 2;
            System.out.println("2^" + i + "= " + mul);
        }

        System.out.println("OJO CON LAS EVALUACIONES DE N! ");
        mul = 1; //prueba también escribiendo long o double en lugar de int
        for (int i = 1; i < 32; i++) {
            mul = mul * i;
            System.out.println(i + "!= " + mul);
        }

        // Básicamente se desborda la variable mul, cuya precisión no puede exceder,
        // para valores positivos, el valor 2^31-1
    }
}
