

package anmPractica1;

import auxiliar.Matrices;
import ezequiel.GeneradorMatriz;
import ezequiel.Matriz;
import ezequiel.MatrizReal;

/**
 * @author MILANOP01
 */
public class Ejercicio1_3 {


    public static void main(String[] args) {
        int n = 9;
        // Matrices de Hilbert
        final Matriz<Double>[] Hn = new MatrizReal[n];
        for (int i = 0; i < Hn.length; i++) {
            Hn[i] = Hilbert(i + 1);
        }
        // Matrices b
        final Matriz[] b = new MatrizReal[n];
        for (int i = 0; i < b.length; i++) {
            final int b_i = i;
            b[i] = new MatrizReal(i + 1,1, new GeneradorMatriz<Double>() {
                @Override
                public Double get(int fila, int columna) {
                    Double v = Hn[b_i].get(fila,0);
                    for (int k = 1; k < Hn[b_i].filas(); k++) {
                        v = v + Hn[b_i].get(fila,k);
                    }
                    return v;
                }
            });
        }
        try {
            //Apartado 1.
            for (int i = 0; i < n; i++) {
                System.out.println("\n \t i = " + i + "\n Solución Hn[" + i + "] u = b:");
                Matriz solucion = Hn[i].solveCramer(b[i]);
                System.out.println(" u = " + solucion);
                Matriz resto = Hn[i].producto(solucion).resta(b[i]);
                System.out.println("||Hn[" + i + "]u-b|| = " + resto);
            }
            // Apartado 2.
            System.out.println("----------------------------------------");
            for (int i = 0; i < n; i++) {
                Matriz b_pert = b[i].suma(Perturbacion(b[i].filas(),b[i].columnas()));
                System.out.println("\n \t i = " + i + "\n Solución Hn[" + i + "] u = b:");
                Matriz solucion = Hn[i].solveCramer(b_pert);
                System.out.println(" u = " + solucion);
                Matriz resto = Hn[i].producto(solucion).resta(b[i]);
                System.out.println("||Hn[" + i + "]u-b|| = " + resto);
            }
            // Apartado 3: preguntar profesor
            // Sobre como generar la matriz inversa
            for (int i = 1; i <= 20; i++) {
                Matriz h = Hilbert(i);
                double cond = h.norma1()*h.inversa().norma1();
                System.out.println("{ " + cond + " }");
            }
        } catch (Matrices.ErrorMatrices ignored) {

        }

    }


    public static Matriz Hilbert(int n) {
        return new MatrizReal(n,n, new GeneradorMatriz<Double>() {
            @Override
            public Double get(int fila, int columna) {
                return 1. / (fila+columna+1.);
            }
        });
    }

    public static Matriz Perturbacion(int filas, int columnas) {
        return new MatrizReal(filas, columnas, new GeneradorMatriz<Double>() {
            @Override
            public Double get(int fila, int columna) {
                return (fila+columna % 2 == 0 ? 10E-6 : -10E-6);
            }
        });
    }


}


