package anmPractica1;

import auxiliar.Matrices;
import ezequiel.Matriz;
import ezequiel.MatrizReal;

/**
 * Created by lilezek on 10/04/15.
 */
public class Ejercicio1_5 {
    public static void main(String[] args) {
        try {
            Matriz A = new MatrizReal(4,6,
                    1.,  3., -5. , 2. , 8. ,  2.,
                    5., -2., -1. , 8. , 1. , -3.,
                    4.,  3., -16., 23., 19., -4.,
                    9.,  2., -1. , -1., 7. ,  4.);
            Matriz B = A.traspuesta().gramSchmidt().traspuesta();
            // Preguntar profesor: La última fila debería ser 0
            System.out.println(B);
            System.out.println(B.producto(B.traspuesta()));
        } catch (Matrices.ErrorMatrices errorMatrices) {
            errorMatrices.printStackTrace();
        }
    }
}
