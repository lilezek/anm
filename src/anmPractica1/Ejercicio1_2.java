package anmPractica1;

import auxiliar.Matrices;
import ezequiel.Matriz;
import ezequiel.MatrizReal;

/**
 * Created by lilezek on 1/03/15.
 */
public class Ejercicio1_2 {
    public static void main(String[] args) {

        Matriz A = new MatrizReal(4, 4,
                10., 7., 8., 7.,
                7., 5., 6., 5.,
                8., 6., 10., 9.,
                7., 5., 9., 10.);

        System.out.println(A);

        Matriz invA = new MatrizReal(4,4,
                25., -41., 10., -6.,
                -41., 68., -17., 10.,
                10., -17., 5., -3.,
                -6., 10., -3., 2.);

        double e = 0.1;

        Matriz DeltaA = new MatrizReal(4, 4,
                0.       , 0.       , 0.1 * e  , 0.2 * e,
                0.08 * e , 0.04 * e , 0.       , 0.     ,
                0.       , -0.02 * e, -0.11 * e, 0.     ,
                -0.01 * e, -0.01 * e, 0.       , -0.02 * e);

        Matriz b  = new MatrizReal(4, 1,
                32., 23., 33., 31.);
        Matriz bP = new MatrizReal(4, 1,
                32 + 0.1 * e, 23 - 0.1 * e, 33 + 0.1 * e, 31 - 0.1 * e);

        try
        {

            double detA = A.determinante();
            System.out.println("Determinante de A = " + detA);

            System.out.println("Solución Au = b:");
            Matriz solucion = A.solveCramer(b);
            System.out.println(solucion);

            Matriz resto = A.producto(solucion).resta(b);

            System.out.println(resto);

            System.out.println("||Au-b|| = " + resto.norma1());
            System.out.println("Solución Bv = b:");

            Matriz B = A.suma(DeltaA);
            Matriz solucionB = B.solveCramer(b);
            System.out.println(" v = " + solucionB);

            resto = B.producto(solucionB).resta(b);
            System.out.println("||Bv-b|| = " + resto.norma1());
            System.out.println("" +
                    "Una pequeña perturbación de A (B=A+Delta(A)) produce una\n"
                    + "gran perturbación en la solucion del sistema ");

            // 1. Comprobar que la inversa de A es invA:
            Matriz test = A.inversa();
            if (test.igual(invA,5E-16))
                System.out.println("invA es la inversa de A");
            else
                System.out.println("invA no es la inversa de A");

            // 2. Implementar los métodos auxiliares ya está hecho.

            // 3. Calcular los números de condición (mal):
            Double cond1 = A.norma1()*invA.norma1();
            Double condinf = A.normainf() * invA.normainf();

            System.out.println("Numero de condicion 1: "+cond1);
            System.out.println("Numero de conficion infinito: "+condinf);

        } catch (Matrices.ErrorMatrices ex) {
            ex.printStackTrace();
        }

    }
}
