/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package auxiliar;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author antoniopallaresruiz
 */
public class MetodosFunciones {


    public static double[][] tablaGrafica(Funcion f, int numpuntos, double a, double b) {
        double[][] tabla = new double[numpuntos][2];
        for (int i = 0; i < tabla.length; i++) {
            tabla[i][0] = a + i * (b - a) / (numpuntos - 1);
            tabla[i][1] = f.eval(tabla[i][0]);

        }
        return tabla;
    }

    public static double[][] tablaGraficaTchev(Funcion f,
                                               int N, double a, double b) {
        double[][] tabla = new double[N][2];
        for (int i = 0; i < tabla.length; i++) {
            tabla[i][0] = a + (Math.cos((2 * i + 1) * Math.PI / (2 * N)) + 1) / 2 * (b - a);
            tabla[i][1] = f.eval(tabla[i][0]);

        }
        return tabla;
    }

    public static class suma implements Funcion {
        Funcion f;
        Funcion g;

        public suma(Funcion f1, Funcion g1) {
            f = f1;
            g = g1;
        }

        public double eval(double x) {
            return f.eval(x) + g.eval(x);
        }
    }

    public static class producto implements Funcion {
        Funcion f;
        Funcion g;

        public producto(Funcion f1, Funcion g1) {
            f = f1;
            g = g1;
        }

        public double eval(double x) {
            return f.eval(x) * g.eval(x);
        }
    }

    public static class diferencia implements Funcion {
        Funcion f;
        Funcion g;

        public diferencia(Funcion f1, Funcion g1) {
            f = f1;
            g = g1;
        }

        public double eval(double x) {
            return f.eval(x) - g.eval(x);
        }
    }

    public static class error implements Funcion {
        Funcion f;
        Funcion g;

        public error(Funcion f1, Funcion g1) {
            f = f1;
            g = g1;
        }

        public double eval(double x) {
            return Math.abs(f.eval(x) - g.eval(x));
        }
    }

    public static class composicion implements Funcion {
        Funcion f;
        Funcion g;

        public composicion(Funcion f1, Funcion g1) {
            f = f1;
            g = g1;
        }

        public double eval(double x) {
            return f.eval(g.eval(x));
        }
    }

    public static class DerivadaAproximadaN implements Funcion {

        Funcion f;
        double paso;
        int N;

        public DerivadaAproximadaN(Funcion f, double paso, int N) {
            this.f = f;
            this.paso = paso;
            this.N = N;
        }

        @Override
        public double eval(double x) {
            double[][] listaPuntos = MetodosFunciones.tablaGrafica(f, N + 1, x - (N / 2) * paso, x + (N / 2) * paso);
            PolinomioInterpolador p = new PolinomioInterpolador(listaPuntos);

            return p.derivada().eval(x);
        }
    }


    public static class derivada2puntos implements Funcion {
        Funcion f;
        double h; //paso

        public derivada2puntos(Funcion f, double h) {
            this.f = f;
            this.h = h;
        }

        @Override
        public double eval(double x) {
            return (f.eval(x + h) - f.eval(x)) / h;
        }
    }

    public static class derivada3puntos implements Funcion {
        Funcion f;
        double h; //paso

        public derivada3puntos(Funcion f, double h) {
            this.f = f;
            this.h = h;
        }

        @Override
        public double eval(double x) {
            return (f.eval(x + h) - f.eval(x - h)) / (2 * h);
        }
    }

    public static class derivadaSegunda3puntos implements Funcion {
        Funcion f;
        double h; //paso

        public derivadaSegunda3puntos(Funcion f, double h) {
            this.f = f;
            this.h = h;
        }

        @Override
        public double eval(double x) {
            return (f.eval(x + h) - 2 * f.eval(x) + f.eval(x - h)) / (h * h);
        }
    }

    public static class derivada5puntos implements Funcion {
        Funcion f;
        double h; //paso

        public derivada5puntos(Funcion f, double h) {
            this.f = f;
            this.h = h;
        }

        @Override
        public double eval(double x) {
            return (f.eval(x - 2 * h) - 8 * f.eval(x - h) + 8 * f.eval(x + h) - f.eval(x + 2 * h)) / (12 * h);
        }
    }

    public static class derivadaExtrapolacion implements Funcion {
        double derivada;
        double h;
        int k = 1;
        boolean imprimepasos = false;
        Funcion f;

        public derivadaExtrapolacion(Funcion f, double h) {
            this.f = f;
            this.h = h;
        }

        public derivadaExtrapolacion(Funcion f, double h, int k, boolean imprimepasos) {
            this.f = f;
            this.h = h;
            this.k = k;
            this.imprimepasos = imprimepasos;
        }

        @Override
        public double eval(double x) {
            double[][] D = new double[k + 1][];
            double paso = h;
            for (int n = 0; n <= k; n++) {
                D[n] = new double[n + 1];
                derivada3puntos f3 = new derivada3puntos(f, paso);
                D[n][0] = f3.eval(x);
                paso = paso / 2;
            }
            double sde = 1;
            for (int j = 1; j <= k; j++) {
                sde = 4 * sde;
                for (int n = j; n <= k; n++) {

                    D[n][j] = D[n][j - 1] + (D[n][j - 1] - D[n - 1][j - 1]) / (sde - 1);

                }


            }
            if (imprimepasos) {
                System.out.println("n \t" + "      D[n][0]    " + "  \t     D[n][1]" + " \t    D[n,2] "
                        + "\t    .... ");
                for (int n = 0; n < k; n++) {
                    System.out.println(n + "\t " + MetodosListas.toString(D[n]));
                    ;
                }
            }

            return D[k][k];
        }
    }


    /**
     * Metodo que implementa la regla compuesta del trapecio en el intervalo
     * [a,b] usando N intervalos ( h=(b-a)/N )
     *
     * @param f
     * @param a
     * @param b
     * @param N
     * @return
     */

    public static double integralTrapecio(Funcion f, double a, double b, int N) {
        double suma = f.eval(a) + f.eval(b);
        double h = (b - a) / N;
        for (int i = 1; i < N; i++) {
            suma = suma + 2 * f.eval(a + i * h);

        }

        return suma * h / 2;
    }

    /**
     * Metodo que implementa la regla compuesta de Simpson 1/3 utilizando 2N
     * divisiones del intervalo $[a,b]$  ( h= (b-a)/(2N) )
     *
     * @param f
     * @param a
     * @param b
     * @param N
     * @return
     */

    public static double integralSimpson(Funcion f, double a, double b, int N) {
        double suma = f.eval(a) + f.eval(b);
        double h = (b - a) / (2 * N);
        for (int i = 1; i <= N; i++) {
            suma = suma + 4 * f.eval(a + (2 * i - 1) * h);
        }
        for (int i = 1; i < N; i++) {
            suma = suma + 2 * f.eval(a + 2 * i * h);
        }

        return suma * h / 3;
    }

    /**
     * Metodo que implementa la regla compuesta que usa la cuadratura de Gauss
     * de 5 puntos en N divisiones del intervalo $[a,b]$  ( h= (b-a)/N )
     *
     * @param f
     * @param a
     * @param b
     * @param N
     * @return
     */
    public static double integralGauss(Funcion f, double a, double b, int N) {
        double[] x = new double[5];
        double[] c = new double[5];
        // nodos y pesos en [-1,1]
        x[2] = 0.0;
        c[2] = 0.568888888888889;
        x[1] = -0.538469310105683;
        c[1] = 0.478628670499366;
        x[3] = 0.538469310105683;
        c[3] = 0.478628670499366;
        x[0] = -0.906179845938664;
        c[0] = 0.236926885056189;
        x[4] = 0.906179845938664;
        c[4] = 0.236926885056189;


        double suma = 0;
        double h = (b - a) / N;
        for (int i = 0; i < N; i++) {
            double ai = a + i * h;
            for (int j = 0; j < 5; j++) {
                suma = suma + c[j] * f.eval(ai + (1 + x[j]) / 2 * h);
            }
        }
        return suma * h / 2;
    }

    /**
     * Montecarlo dimension 1 con N evaluaciones
     *
     * @param f
     * @param a
     * @param b
     * @param N
     * @return
     */
    public static double intMonteCarlo1(Funcion f, double a, double b, int N) {
        double suma = 0;

        for (int i = 0; i < N; i++) {
            suma = suma + f.eval(a + Math.random() * (b - a));

        }


        return suma / N * (b - a);
    }

           /*
    Lleva a tu clase auxiliar.MetodosFunciones los metodos que te falten


    */


    /**
     * Metodo para construir una lista de numpuntos puntos de la grafica de una
     * curva dada en coordenadas parametricas (x(t),y(t)) tomando numpuntos valores
     * de t equiespaciados en [t0,t1]
     *
     * @param x
     * @param y
     * @param numpuntos
     * @param t0
     * @param t1
     * @return
     */
    public static double[][] tablaGrafParam(Funcion x, Funcion y, int numpuntos,
                                            double t0, double t1) {
        double[][] tabla = new double[numpuntos][2];
        for (int i = 0; i < tabla.length; i++) {
            double t = t0 + i * (t1 - t0) / (numpuntos - 1);
            tabla[i][0] = x.eval(t);
            tabla[i][1] = y.eval(t);
        }
        return tabla;
    }


    /**
     * Metodo para aproximar los valores minimo y maximo de una funcion f en un intervalo
     * viendo el minimo y el maximo de los valores de f en una lista de numpuntos puntos
     * equidistribuidos en [a,b].
     *
     * @param f
     * @param numpuntos
     * @param a
     * @param b
     * @return devuelve un vector {m,M} con las aproximaciones al minimo y al maximo de f
     */
    public static double[] extremos(Funcion f, int numpuntos, double a, double b) {
        List<Double> tabla = new ArrayList<Double>();
        for (int i = 0; i < numpuntos; i++) {
            double x = a + i * (b - a) / (numpuntos - 1);
            tabla.add(f.eval(x));

        }
        Collections.sort(tabla);
        double[] extremos = new double[2];
        extremos[0] = tabla.get(0);
        extremos[1] = tabla.get(numpuntos - 1);
        return extremos;
    }


    /**
     * (entrega de A. Juarez, sigue el algoritmo del libro de burden-faires)
     * Implementación del algoritmo de la integral adaptativa de una función f,
     * en un intervalo [a,b], con una precisión(Tol),con una profundidad maxima de
     * N subdivisiones del
     * intervalo.
     *
     * @param f
     * @param a
     * @param b
     * @param Tol
     * @param N
     * @return
     */
    public static double IntegralAdaptativa(Funcion f, double a, double b, double Tol, int N) {
        //Inicializamos un contador para llevar las evaluaciones que se hacen de la función
        //y también creamos e inicializamos los arrays que necesitamos con los datos que nos da el
        //artículo.En la variable I guardaremos el valor de la integral.

        int evaluaciones = 0;
        double I = 0;  //para ir acumulando las integrales
        double[] TOL = new double[N];
        double[] A = new double[N];
        double[] h = new double[N];
        double[] FA = new double[N];
        double[] FC = new double[N];
        double[] FB = new double[N];
        double[] S = new double[N];
        double[] L = new double[N];
        double[] v = new double[8];
        int i = 1;
        TOL[0] = 10 * Tol;
        A[0] = a;
        h[0] = (b - a) / 2.;
        FA[0] = f.eval(a);
        FC[0] = f.eval(a + h[i - 1]);
        FB[0] = f.eval(b);
        evaluaciones += 3;
        S[0] = h[i - 1] / 3. * (FA[i - 1] + 4 * FC[i - 1] + FB[i - 1]);
        L[0] = 1.;

        //Variable booleana de control de flujo.Para parar la ejecución si se da el caso
        boolean stop = false;
        while (i > 0 & !stop) {
            //Entramos en el bucle donde implementamos el algoritmo que nos da el artículo ,al
            //mismo tiempo que vamos sumando al contador de evaluaciones si es necesario.
            double FD = f.eval(A[i - 1] + h[i - 1] / 2);
            double FE = f.eval(A[i - 1] + 3 * h[i - 1] / 2);
            evaluaciones += 2;
            double S1 = h[i - 1] / 6 * (FA[i - 1] + 4 * FD + FC[i - 1]);
            double S2 = h[i - 1] / 6 * (FC[i - 1] + 4 * FE + FB[i - 1]);
            v[0] = A[i - 1];
            v[1] = FA[i - 1];
            v[2] = FC[i - 1];
            v[3] = FB[i - 1];
            v[4] = h[i - 1];
            v[5] = TOL[i - 1];
            v[6] = S[i - 1];
            v[7] = L[i - 1];

            i = i - 1;
            if (Math.abs(S1 + S2 - v[6]) < v[5]) {
                I = I + (S1 + S2);
            } else {
                if (v[7] >= N) {
                    //En este caso devolvemos un mensaje de error
                    //y paramos el flujo de ejecución
                    System.out.println("\n ******ERROR INT ADAPT:"
                            + " NIVEL SOBREPASADO******\n");
                    stop = true;

                } else {
                    i = i + 1;
                    A[i - 1] = v[0] + v[4];
                    FA[i - 1] = v[2];
                    FC[i - 1] = FE;
                    FB[i - 1] = v[3];
                    h[i - 1] = v[4] / 2;
                    TOL[i - 1] = v[5] / 2;
                    S[i - 1] = S2;
                    L[i - 1] = v[7] + 1;
                    i = i + 1;
                    A[i - 1] = v[0];
                    FA[i - 1] = v[1];
                    FC[i - 1] = FD;
                    FB[i - 1] = v[2];
                    h[i - 1] = h[i - 2];
                    TOL[i - 1] = TOL[i - 2];
                    S[i - 1] = S1;
                    L[i - 1] = L[i - 2];


                }

            }


        }
        //Imprimimos las evaluaciones de f y devolvemos el valor de I
        System.out.println("Evaluaciones de f = " + evaluaciones);
        return I;

    }


    /**
     * Metodo para aproximar la derivada k-esima de una funcion f en un punto x0
     * dada una lista de puntos de su grafica, L, usando el polinomio interpolador en n>=k puntos
     * y la derivada k-esima de ese polinomio como aproximacion
     * <p/>
     * Entrega practica de Antonio Juarez
     * <p/>
     * Implementación del algoritmo que nos devuelve una aproximación de la
     * derivada k-ésima de una función en un punto x0.La doble lista L contiene
     * la información relativa a los nodos y su valor que dependerá de la
     * función.
     *
     * @param x0
     * @param k
     * @param n
     * @param L
     * @return
     */
    public static double DerivadaKesima(double x0, int k, int n, double[][] L) {
        //Inicializamos las variables necesarias y en un array gusrdamos los nodos de la lista L
        int M = k;
        int N = n;
        double aproxDeriv = 0;
        double[] puntos = new double[n + 1];
        for (int i = 0; i < n + 1; i++) {
            puntos[i] = L[i][0];

        }
        //Definimos una matriz tridimensional donde ir guardando los valores de los coeficientes
        //y la inicializamos a 0
        double[][][] coef = new double[M + 1][N + 1][N + 1];
        for (int i = 0; i < M + 1; i++) {
            for (int j = 0; j < N + 1; j++) {
                for (int l = 0; l < N + 1; l++) {
                    coef[i][j][l] = 0;

                }
            }
        }

        //Creamos las variables c1,c2 y c3
        double c1, c2, c3;

        //Implementamos el algoritmo que nos da el artículo paso a paso teneindo en cuenta
        //algún que otro requisito que se verá más adelante.
        coef[0][0][0] = 1;
        c1 = 1;
        int m;
        for (int t = 1; t <= N; t++) {
            c2 = 1;
            for (int i = 0; i <= t - 1; i++) {
                c3 = puntos[t] - puntos[i];
                c2 = c2 * c3;

                for (m = 0; m <= Math.min(t, M); m++) {
                    //En el artículo hay una observación de que en el caso de que m=0 hay que hacer unos
                    //apaños ,por eso distinguimos dos casos y en el bucle siguiente igual.
                    //Si no lo hicieramos daría error ya que estaríamos evaluando una posición negativa
                    //en el array(m-1=-1 si m=0).
                    if (m == 0) {
                        coef[m][t][i] = (puntos[t] - x0) * coef[m][t - 1][i] / c3;
                    } else {
                        coef[m][t][i] = ((puntos[t] - x0) * coef[m][t - 1][i] - m * coef[m - 1][t - 1][i]) / c3;
                    }

                }

            }

            for (m = 0; m <= Math.min(t, M); m++) {
                if (m == 0) {
                    coef[m][t][t] = -c1 / c2 * (puntos[t - 1] - x0) * coef[m][t - 1][t - 1];

                } else {
                    coef[m][t][t] = c1 / c2 * (m * coef[m - 1][t - 1][t - 1] - (puntos[t - 1] - x0) * coef[m][t - 1][t - 1]);
                }
            }
            c1 = c2;
        }


        //Una vez que tenemos los coeficientes hacemos las combinaciones lineales de éstos
        //con los datos de la función f recogidos en la segunda columna de la lista L

        for (int i = 0; i <= n; i++) {
            aproxDeriv += coef[k][n][i] * L[i][1];

        }

        //Devolvemos el valor de la derivada
        return aproxDeriv;

    }
    /*
    Lleva a tu clase auxiliar.MetodosFunciones los metodos que te falten


    */


    public static class ErrorDerivada extends Exception {

        public ErrorDerivada() {
        }
    }


}
