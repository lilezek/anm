/*
 * Matrices.java
 *
 * Creado el 31 de enero de 2011, 11:00
 */
package auxiliar;

import ORG.netlib.math.complex.Complex;

/**
 * @author Antonio Pallares
 * @version 2015.0 (inicial)
 *          <p/>
 *          <p/>
 *          ANALISIS NUMERICO MATRICIAL - GRADO EN MATEMATICAS - UNIVERSIDAD DE MURCIA
 */
public class Matrices {

    /**
     * Variable precision
     * utilizada para considerar nulos los numeros de
     * menor tama~no
     */
    static double precision = 5E-16;

    /**
     * El metodo nos dice si la matriz es o no una matriz cuadrada
     */
    public static boolean ifCuadrada(double[][] A) {
        int n = A.length;
        boolean respuesta = true;
        for (int i = 0; i < n; i++) {
            if (A[i].length != n) {
                respuesta = false;
                break;
            }
        }
        return respuesta;
    }

    /**
     * El metodo comprueba si es o no una matriz
     */
    public static boolean ifMatriz(double[][] A) {
        int n = A.length;
        int m = A[0].length;
        boolean respuesta = true;

        for (int i = 0; i < n; i++) {
            if (A[i].length != m) {
                respuesta = false;
                break;
            }
        }
        return respuesta;
    }

    /**
     * Metodo que devuelve una copia del vector v
     *
     * @param v
     * @return
     */
    public static double[] copia(double[] v) {
        int n = v.length;
        double[] cv = new double[n];
        System.arraycopy(v, 0, cv, 0, n);
        return cv;
    }

    public static Complex[] copia(Complex[] v) {
        int n = v.length;
        Complex[] cv = new Complex[n];
        System.arraycopy(v, 0, cv, 0, n);
        return cv;
    }


    /**
     * Metodo que devuelve una copia de la matriz A
     *
     * @param A
     * @return
     */
    public static double[][] copia(double[][] A) {
        int n = A.length;
        double[][] cA = new double[n][];
        for (int i = 0; i < n; i++) {
            cA[i] = copia(A[i]);
        }
        return cA;
    }

    public static Complex[][] copia(Complex[][] A) {
        int n = A.length;
        Complex[][] cA = new Complex[n][];
        for (int i = 0; i < n; i++) {
            cA[i] = copia(A[i]);
        }
        return cA;
    }

    /**
     * Metodo que devuelve la matriz de una fila
     * definida por el vector v
     *
     * @param v
     * @return
     */
    public static double[][] matriz1Fila(double[] v) {
        int n = v.length;
        double[][] fila = new double[1][n];
        for (int i = 0; i < n; i++) {
            fila[0][i] = v[i];
        }
        return fila;
    }

    /**
     * Metodo que devuelve la matriz de una columna
     * definida por el vector v
     *
     * @param v
     * @return
     */
    public static double[][] matriz1Columna(double[] v) {
        int n = v.length;
        double[][] columna = new double[n][1];
        for (int i = 0; i < n; i++) {
            columna[i][0] = v[i];
        }
        return columna;
    }

    /**
     * DEVUELVE la matriz traspuesta de una matriz REAL
     */
    public static double[][] traspuesta(double[][] A)
            throws ErrorMatrices {
        int n;
        int m;
        double[][] At;

        if (!ifMatriz(A)) {
            System.out.println("Error: matriz mal definida");
            throw new ErrorMatrices();
        }

        n = A.length;
        m = A[0].length;
        At = new double[m][n];
        for (int j = 0; j < m; j++) {
            for (int i = 0; i < n; i++) {
                At[j][i] = A[i][j];
            }
        }
        return At;
    }


    /**
     * Devuelve la matriz adjunta de una matriz COMPLEJA
     */
    public static Complex[][] adjunta(Complex[][] A)
            throws ErrorMatrices {
        int n = A.length;
        int m = A[0].length;
        Complex[][] At;

        boolean cuadrada = true;
        for (int i = 1; i < n; i++) {
            if (A[i].length != m) {
                cuadrada = false;
                break;
            }
        }

        if (!cuadrada) {
            System.out.println("Error: matriz mal definida");
            throw new ErrorMatrices();
        }


        m = A[0].length;
        At = new Complex[m][n];
        for (int j = 0; j < m; j++) {
            for (int i = 0; i < n; i++) {
                At[j][i] = A[i][j].conj();
            }
        }
        return At;
    }


    /**
     * El metodo realiza la suma de dos matrices
     */
    public static double[][] suma(double[][] A, double[][] B)
            throws ErrorMatrices {
        int n;
        int m;
        double[][] AmasB;

        if (!ifMatriz(A)) { // ifmatriz(A) == false
            System.out.println("Error: matriz izquierda mal definida");
            throw new ErrorMatrices();
        }
        if (!ifMatriz(B)) {
            System.out.println("Error: matriz derecha mal definida");
            throw new ErrorMatrices();
        }
        n = B.length; // filas de B
        m = B[0].length; // columnas de B
        if ((n != A.length) || (m != A[0].length)) { // comparar con A
            System.out.println("Error: dimensiones incompatibles");
            throw new ErrorMatrices();
        }

        AmasB = new double[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                AmasB[i][j] = A[i][j] + B[i][j];
            }
        }
        return AmasB;
    }

    /**
     * El metodo nos da la suma de dos vectores
     */
    public static double[] suma(double[] u, double[] v)
            throws ErrorMatrices {

        return suma(matriz1Fila(u), matriz1Fila(v))[0];
    }

    /**
     * Resta de dos matrices
     */
    public static double[][] resta(double[][] A, double[][] B)
            throws ErrorMatrices {
        double[][] menosB = producto(-1, B);

        return suma(A, menosB);
    }

    /**
     * Resta de dos vetores
     */
    public static double[] resta(double[] u, double[] v)
            throws ErrorMatrices {
        double[] menosv = producto(-1, v);

        return suma(u, menosv);
    }

    /**
     * Producto de dos matrices
     */
    public static double[][] producto(double[][] A, double[][] B)
            throws ErrorMatrices {
        int n; // filas A
        int m; // columnas A y filas B
        int p; // columnas B
        double[][] AporB;

        if (!ifMatriz(A)) { // ifmatriz(A) == false
            System.out.println("Error: matriz izquierda mal definida");
            throw new ErrorMatrices();
        }
        if (!ifMatriz(B)) {
            System.out.println("Error: matriz derecha mal definida");
            throw new ErrorMatrices();
        }

        if ((A[0].length != B.length)) { // comparar con A
            System.out.println("Error: dimensiones incompatibles");
            throw new ErrorMatrices();
        }

        n = A.length; // filas de A
        m = A[0].length; // (B.length) columnas de A (o filas de B)
        p = B[0].length; // columnas de B

        AporB = new double[n][p];
        for (int i = 0; i < n; i++) {
            for (int k = 0; k < p; k++) {
                //AporB[i][k]=Double.NaN; //esta es una peque~na trampa...;
                for (int j = 0; j < m; j++) {
                    AporB[i][k] = AporB[i][k] + A[i][j] * B[j][k];
                }
            }
        }
        return AporB;
    }

    /**
     * Producto de dos vectores
     *
     * @param u
     * @param v
     * @return (u, v)
     * @throws auxiliar.Matrices.ErrorMatrices
     */
    public static double producto(double[] u, double[] v)
            throws ErrorMatrices {
        return producto(matriz1Fila(u), matriz1Columna(v))[0][0];
    }


    /**
     * Producto de un escalar por un vector
     *
     * @param c escalar
     * @param u vector
     * @return c.u
     */
    public static double[] producto(double c, double[] u) {
        int n = u.length;
        double[] v = new double[n];
        for (int i = 0; i < n; i++) {
            v[i] = c * u[i];
        }

        return v;
    }

    /**
     * Producto de una matriz por un escalar
     */
    public static double[][] producto(double c, double[][] A) {

        int n = A.length;
        double[][] cA = new double[n][];
        for (int i = 0; i < n; i++) {
            cA[i] = producto(c, A[i]);
        }
        return cA;
    }


    /**
     * Producto de una matriz y un vector
     */
    public static double[] producto(double[][] A, double[] v)
            throws ErrorMatrices {

        if (!ifMatriz(A)) {
            System.out.println("Error: matriz mal definida");
            throw new ErrorMatrices();
        }
        int n = A[0].length;
        int m = v.length;
        if (n != m) {
            System.out.println("Error: dimensiones incompatible");
            throw new ErrorMatrices();
        }
//       m = A.length;
//        double[] prod = new double[m];
//        for (int i = 0; i < m; i++) {
//            prod[i] = 0.;
//            for (int j = 0; j < n; j++) {
//                prod[i] = prod[i] + A[i][j] * v[j];
//
//            }
//        }
//
//        return prod;
        return traspuesta(producto(A, matriz1Columna(v)))[0];

    }

    /**
     * Convierte una matriz en String
     */
    public static String toString(double[][] A) {
        return MetodosListas.toString(A);
    }

    /**
     * Convierte un vector a String
     */
    public static String toString(double[] u) {
        return MetodosListas.toString(u);
    }


    /**
     * Escribe una matriz dada
     */
    public static void escribe(double[][] A) {

        System.out.println(MetodosListas.toString(A));

    }

    /**
     * Escribe un vector
     */
    public static void escribe(double[] u) {
        System.out.println(MetodosListas.toString(u));
    }


    /**
     * Calcula la norma1 euclidea de un vector
     */
    public static double norma(double[] u) {
        double norm = 0;

        for (int i = 0; i < u.length; i++) {
            norm = norm + u[i] * u[i];
        }
        return Math.sqrt(norm);
    }

    /**
     * Calcula la norma1 uno de un vector
     */
    public static double norma1(double[] u) {
        double norm = 0;

        for (int i = 0; i < u.length; i++) {
            norm = norm + Math.abs(u[i]);
        }
        return norm;
    }

    /**
     * Calcula la norma1 del supremo de un vector
     */
    public static double normaInfinito(double[] u) {
        double norm = 0;

        for (int i = 0; i < u.length; i++) {
            double x = Math.abs(u[i]);
            if (norm < x) {
                norm = x;
            }
        }
        return norm;
    }


    /**
     * Metodo que proporciona el menor de la matriz A al eliminar la fila i y la
     * columna j
     */
    public static double[][] menor(double[][] A, int i, int j)
            throws ErrorMatrices {

        if (!ifCuadrada(A)) {
            System.out.println("Error: matriz no cuadrada");
            throw new ErrorMatrices();
        }

        int n = A.length;
        if ((i >= n) || (i < 0) || (j >= n) || (j < 0)) {
            System.out.println("Error: indices no compatibles");
            throw new ErrorMatrices();
        }


        if (n == 1) {
            return new double[1][1];
        } else {
            double[][] Aux = new double[n - 1][n - 1];

            for (int m = 0; m < i; m++) {
                for (int k = 0; k < j; k++) {
                    Aux[m][k] = A[m][k];
                }
                for (int k = j; k < n - 1; k++) {
                    Aux[m][k] = A[m][k + 1];
                }
            }
            for (int m = i; m < n - 1; m++) {
                for (int k = 0; k < j; k++) {
                    Aux[m][k] = A[m + 1][k];
                }
                for (int k = j; k < n - 1; k++) {
                    Aux[m][k] = A[m + 1][k + 1];
                }
            }

            return Aux;
        }
    }

    /**
     * Método para calcular el determinante desarrollando por filas.
     *
     * @param A = matriz cuadrada
     * @return determinante
     * @throws auxiliar.Matrices.ErrorMatrices si no es cuadrada
     */
    public static double determinanteMenores(double[][] A) throws ErrorMatrices {
        double determinante = 0;
        if (ifCuadrada(A) == false) {
            System.out.println("Error: Matriz no cuadrada");
            throw new ErrorMatrices();
        }
        double signo = 1;
        if (A.length == 1) {
            return A[0][0];
        }
        for (int j = 0; j < A.length; j++) {
            double[][] menor = menor(A, 0, j);
            determinante = determinante + signo * determinanteMenores(menor) * A[0][j];
            signo = -1 * signo;
        }

        return determinante;
    }

    /**
     * Método de Cramer para resolver Ax=b
     *
     * @param A
     * @param b
     * @return
     * @throws auxiliar.Matrices.ErrorMatrices
     */
    public static double[] Cramer(double[][] A, double[] b) throws ErrorMatrices {
        int n = A.length;
        double determinante = determinanteMenores(A);
        if (determinante == 0) {
            System.out.println("La matriz es singular");
            throw new ErrorMatrices();
        }
        double[] solucion = new double[n];
        for (int i = 0; i < n; i++) {
            double[][] Aux = new double[n][n];
            for (int k = 0; k < n; k++) {
                for (int j = 0; j < n; j++) {
                    if (j == i) {
                        Aux[k][j] = b[k];
                    } else {
                        Aux[k][j] = A[k][j];
                    }

                }

            }
            solucion[i] = determinanteMenores(Aux) / determinante;

        }

        return solucion;
    }


    /**
     * Excepcion ErrorMatrices para ser devuelta por los metodos de la clase
     */
    public static class ErrorMatrices extends Exception {

        public ErrorMatrices() {
            super("Error: Matrices ");
        }

        public ErrorMatrices(String s) {
            super(s);
        }
    }
}
