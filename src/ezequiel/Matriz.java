package ezequiel;

import auxiliar.Matrices;

import java.util.Vector;

public interface Matriz<T extends Number> extends Cloneable {
    T get(int fila, int columna);
    void set(int fila, int columna, T valor);
    Vector<T> getFila(int fila);
    Vector<T> getColumna(int columna);

    int filas();
    int columnas();

    Matriz<T> submatriz(int f, int c, int f2, int c2);

    boolean cuadrada();

    boolean simetrica() throws Matrices.ErrorMatrices;

    Matriz<T> producto(Matriz<T> b) throws Matrices.ErrorMatrices;

    Matriz<T> producto(T escalar);

    Matriz<T> resta(Matriz<T> b) throws Matrices.ErrorMatrices;
    Matriz<T> suma(Matriz<T> deltaA) throws Matrices.ErrorMatrices;

    Matriz<T> solveCramer(Matriz<T> b) throws Matrices.ErrorMatrices;

    Matriz<Double> ascendente(Matriz<Double> b) throws Matrices.ErrorMatrices;

    Matriz<Double> descendente(Matriz<Double> b) throws Matrices.ErrorMatrices;

    Matriz<T> traspuesta();
    Matriz<T> inversa() throws Matrices.ErrorMatrices;

    boolean igual(Matriz<T> b, double eps);
    double determinante() throws Matrices.ErrorMatrices;

    double norma1();
    double normainf();

    Matriz<T> gramSchmidt();

    Matriz<T>[] LUdoolittle() throws Matrices.ErrorMatrices;
    T determinanteLU() throws Matrices.ErrorMatrices;

    Matriz<T> factorLdeCholeski() throws Matrices.ErrorMatrices;
    Matriz<T> solveCholeski(Matriz<T> b) throws Matrices.ErrorMatrices;
    Matriz<T> solveGaussP(Matriz<T> b) throws Matrices.ErrorMatrices;

    public Matriz<T> clone() throws CloneNotSupportedException;

}
