package ezequiel;

/**
 * Created by lilezek on 1/03/15.
 */
public interface GeneradorMatriz<T> {
    T get(int fila, int columna);
}
