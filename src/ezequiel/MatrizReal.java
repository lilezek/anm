package ezequiel;

import auxiliar.Matrices;

import java.text.DecimalFormat;
import java.util.Vector;

/**
 * Created by lilezek on 1/03/15.
 */
public class MatrizReal implements Matriz<Double>, Cloneable {

    public static final double eps = 1E-10;
    public static final int dec_rep = 2;

    double valores[][];
    String tostr;

    public MatrizReal(int filas, int columnas, double ... valores) {
        if (valores.length != filas*columnas)
            throw new IllegalArgumentException("Número de valores erróneo: se esperaban "+filas*columnas+" y se han escrito "+valores.length);
        this.valores = new double[filas][columnas];
        this.tostr = null;
        for (int i = 0; i < filas; i++)
            for (int j = 0; j < columnas; j++)
                this.valores[i][j] = valores[i*columnas+j];
    }

    public MatrizReal(double[][] valores) {
        int filas = valores.length;
        int columnas = valores[0].length;
        this.valores = new double[filas][columnas];
        for (int i = 0; i < filas; i++)
            for (int j = 0; j < columnas; j++)
                this.valores[i][j] = valores[i][j];
    }


    public MatrizReal(int filas, int columnas, GeneradorMatriz<Double> gen) {
        this.valores = new double[filas][columnas];
        for (int i = 0; i < filas; i++)
            for (int j = 0; j < columnas; j++)
                this.valores[i][j] = gen.get(i,j);
    }

    private MatrizReal(int filas, int columnas) {
        this.valores = new double[filas][columnas];
    }

    @Override
    public Double get(int fila, int columna) {
        return this.valores[fila][columna];
    }

    @Override
    public void set(int fila, int columna, Double valor) {
        this.valores[fila][columna] = valor;
        this.tostr = null;
    }

    @Override
    public Vector<Double> getFila(int fila) {
        return this.box(this.valores[fila]);
    }

    @Override
    public Vector<Double> getColumna(int columna) {
        Vector<Double> r = new Vector<Double>(this.valores.length);
        for (int i = 0; i < this.valores.length; i++)
            r.add(this.valores[i][columna]);
        return r;
    }

    @Override
    public String toString() {
        DecimalFormat df = new DecimalFormat("0."+ new String(new char[dec_rep]).replace("\0","0"));

        String result = "/";
        for (int f = 0; f < this.filas(); f++) {
            if (f == this.filas()-1) {
                result += "\\";
            }
            else if (f > 0) {
                result += "|";
            }
            result += df.format(this.get(f,0));
            for (int c = 1; c < this.columnas(); c++) {
                result += ";\t"+df.format(this.get(f,c));
            }
            result += "\t";
            if (f == 0){
                result += "\\\n";
            }
            else if (f < this.filas()-1) {
                result += "|\n";
            }
        }
        return result+"/";
    }

    @Override
    public double determinante() throws Matrices.ErrorMatrices {
        if (!this.cuadrada()) {
            throw new Matrices.ErrorMatrices("La matriz debe ser cuadrada");
        }
        return Matrices.determinanteMenores(this.valores);
    }

    @Override
    public double norma1() {
        double norm = 0;

        for (int c = 0; c < this.columnas(); c++) {
            double col = 0.;
            for (int f = 0; f < this.filas(); f++) {
                col += Math.abs(this.get(f,c));
            }
            if (col > norm)
                norm = col;
        }
        return norm;
    }

    @Override
    public double normainf() {
        double norm = 0;

        for (int f = 0; f < this.filas(); f++) {
            double fila = 0.;
            for (int c = 0; c < this.columnas(); c++) {
                fila += Math.abs(this.get(f,c));
            }
            if (fila > norm)
                norm = fila;
        }
        return norm;
    }

    @Override
    public Matriz<Double> gramSchmidt() {
        Matriz<Double> result = new MatrizReal(this.filas(), this.columnas());

        // El primer vector es una copia:
        for (int f = 0; f < this.filas(); f++) {
            result.set(f, 0, this.get(f,0));
        }
        // El resto se calcula con las fórmulas
        for (int c = 1; c < this.columnas(); c++) {
            // Copiar el vector columna c-ésimo:
            for (int f = 0; f < this.filas(); f++) {
                result.set(f, c, this.get(f,c));
            }
            // Restarle las proyecciones de los vectores anteriores:
            Matriz<Double> V = this.submatriz(0, c, this.filas()-1, c);
            for (int u = 0; u < c; u++) {
                // U_c = V_c - proj(U_1, V_c) - proj(U_2, V_c) ... proj(U_c-1, V_c)
                Matriz<Double> U = result.submatriz(0, u, result.filas()-1, u);
                try {
                    // proj(u,v) = <u,v>/<u,u> * u
                    double u_v = U.traspuesta().producto(V).get(0,0);
                    double u_u = U.traspuesta().producto(U).get(0,0);
                    for (int f = 0; f < this.filas(); f++) {
                        result.set(f, c, result.get(f,c)-U.get(f,0)*(u_v/u_u));
                    }
                } catch (Matrices.ErrorMatrices errorMatrices) {
                    errorMatrices.printStackTrace();
                }
            }
        }
        // Por último, normalizar los vectores
        for (int c = 0; c < result.columnas(); c++) {
            Matriz<Double> U = result.submatriz(0, c, result.filas() - 1, c);
            try {
                double norm = Math.sqrt(U.traspuesta().producto(U).get(0, 0));
                System.out.println(norm);
                for (int f = 0; f < this.filas(); f++) {
                    result.set(f, c, result.get(f, c) / norm);
                }
            } catch (Matrices.ErrorMatrices errorMatrices) {
                errorMatrices.printStackTrace();
            }
        }
        return result;
    }

    @Override
    public Matriz<Double> submatriz(final int f, final int c, int f2, int c2) {
        if (c > c2 || f > f2)
            return null;

        final Matriz<Double> this2 = this;
        Matriz<Double> result = new MatrizReal(f2-f+1, c2-c+1, new GeneradorMatriz<Double>() {
            @Override
            public Double get(int fila, int columna) {
                return this2.get(fila+f, columna+c);
            }
        });
        return result;
    }

    @Override
    public boolean cuadrada() {
        return (this.valores != null && this.valores.length >= 1 && this.valores.length == this.valores[0].length);
    }

    @Override
    public boolean simetrica() throws Matrices.ErrorMatrices {
        if (!this.cuadrada())
            throw new Matrices.ErrorMatrices("Matriz no cuadrada");
        boolean result = true;
        for (int f = 0; f < this.filas(); f++) {
            for (int c = f+1; c < this.columnas(); c++) {
                if (Math.abs(this.get(f,c) - this.get(c,f)) > eps)
                    result = false;
            }
        }
        return result;
    }

    @Override
    public Matriz<Double> producto(Matriz<Double> rhs) throws Matrices.ErrorMatrices {
        int n; // filas A
        int m; // columnas A y filas B
        int p; // columnas B
        double[][] AporB;

        if (this.columnas() != rhs.filas()) {
            throw new Matrices.ErrorMatrices("dimensiones incompatibles (" +
                    this.filas()+","+this.columnas()+") x ("+rhs.filas()+","+rhs.columnas()+")");
        }

        n = this.filas();
        m = this.columnas();
        p = rhs.columnas();

        AporB = new double[n][p];
        for (int i = 0; i < n; i++) {
            for (int k = 0; k < p; k++) {
                //AporB[i][k]=Double.NaN; //esta es una peque~na trampa...;
                for (int j = 0; j < m; j++) {
                    AporB[i][k] = AporB[i][k] + this.get(i,j) * rhs.get(j,k);
                }
            }
        }
        return new MatrizReal(AporB);
    }

    @Override
    public Matriz<Double> producto(final Double escalar) {
        final Matriz<Double> this2 = this;
        return new MatrizReal(this.filas(), this.columnas(), new GeneradorMatriz<Double>() {
            @Override
            public Double get(int fila, int columna) {
                return this2.get(fila, columna)*escalar;
            }
        });
    }

    @Override
    public Matriz<Double> resta(final Matriz<Double> rhs) throws Matrices.ErrorMatrices {
        if (this.columnas() != rhs.columnas() || this.filas() != rhs.filas()) {
            throw new Matrices.ErrorMatrices("dimensiones incompatibles");
        }
        final Matriz<Double> this2 = this;
        return new MatrizReal(this.filas(), this.columnas(), new GeneradorMatriz<Double>() {
            @Override
            public Double get(int fila, int columna) {
                return this2.get(fila, columna)-rhs.get(fila,columna);
            }
        });
    }

    @Override
    public Matriz<Double> suma(final Matriz<Double> rhs) throws Matrices.ErrorMatrices {
        if (this.columnas() != rhs.columnas() || this.filas() != rhs.filas()) {
            throw new Matrices.ErrorMatrices("dimensiones incompatibles");
        }
        final Matriz<Double> this2 = this;
        return new MatrizReal(this.filas(), this.columnas(), new GeneradorMatriz<Double>() {
            @Override
            public Double get(int fila, int columna) {
                return this2.get(fila, columna)+rhs.get(fila,columna);
            }
        });
    }

    @Override
    public int filas() {
        return this.valores.length;
    }

    @Override
    public int columnas() {
        return this.valores[0].length;
    }

    private double[] unbox(Vector<Double> b) {
        double[] r = new double[b.size()];
        for (int i = 0; i < b.size(); i++)
            r[i] = b.get(i);
        return r;
    }

    private Vector<Double> box(double[] b) {
        Vector<Double> r = new Vector<Double>(b.length);
        for (int i = 0; i < b.length; i++)
            r.add(b[i]);
        return r;
    }

    @Override
    public Matriz<Double> solveCramer(Matriz<Double> b) throws Matrices.ErrorMatrices {
        if (!this.cuadrada()) {
            throw new Matrices.ErrorMatrices("La matriz de solveCramer debe ser cuadrada");
        }
        if (b.columnas() > 1) {
            throw new Matrices.ErrorMatrices("La matriz soluciones debe ser columna");
        }
        if (this.filas() != b.filas()) {
            throw new Matrices.ErrorMatrices("Tamaños incompatibles");
        }
        int n = this.filas();
        double determinante = this.determinante();
        if (determinante < eps) {
            throw new Matrices.ErrorMatrices("La matriz es singular");
        }
        MatrizReal solucion = new MatrizReal(n,1);
        double[][] Aux = new double[n][n];
        for (int i = 0; i < n; i++) {
            for (int k = 0; k < n; k++) {
                for (int j = 0; j < n; j++) {
                    if (j == i) {
                        Aux[k][j] = b.get(k,0);
                    } else {
                        Aux[k][j] = this.get(k,j);
                    }

                }

            }
            solucion.valores[i][0] = Matrices.determinanteMenores(Aux) / determinante;
        }

        return solucion;
    }

    @Override
    public Matriz<Double> ascendente(Matriz<Double> b) throws Matrices.ErrorMatrices {
        if (!this.cuadrada()) {
            throw new Matrices.ErrorMatrices("La matriz de solveCramer debe ser cuadrada");
        }
        if (b.columnas() > 1) {
            throw new Matrices.ErrorMatrices("La matriz soluciones debe ser columna");
        }
        if (this.filas() != b.filas()) {
            throw new Matrices.ErrorMatrices("Tamaños incompatibles");
        }
        int n = this.filas();
        Matriz<Double> solucion = new MatrizReal(n,1);
        for (int i = n - 1; i >= 0; i--) {
            // El determinante de una matriz U equivale al producto de la diagonal
            // Si un elemento de la diagonal es 0, entonces lo es su determinante.
            if (Math.abs(this.get(i,i)) < eps) {
                throw new Matrices.ErrorMatrices("Esta matriz es singular");
            }
            solucion.set(i,0,0.);
            for (int j = i + 1; j < n; j++) {
                solucion.set(i,0, solucion.get(i,0) + this.get(i,j)*solucion.get(j,0));
            }
            solucion.set(i,0, (b.get(i,0) - solucion.get(i,0))/this.get(i,i));
        }
        return solucion;
    }

    @Override
    public Matriz<Double> descendente(Matriz<Double> b) throws Matrices.ErrorMatrices {
        if (!this.cuadrada()) {
            throw new Matrices.ErrorMatrices("La matriz de solveCramer debe ser cuadrada");
        }
        if (b.columnas() > 1) {
            throw new Matrices.ErrorMatrices("La matriz soluciones debe ser columna");
        }
        if (this.filas() != b.filas()) {
            throw new Matrices.ErrorMatrices("Tamaños incompatibles");
        }
        int n = this.filas();
        Matriz<Double> solucion = new MatrizReal(n,1);
        for (int i = 0; i < n; i++) {
            // El determinante de una matriz L equivale al producto de la diagonal
            // Si un elemento de la diagonal es 0, entonces lo es su determinante.
            if (Math.abs(this.get(i,i)) < eps) {
                throw new Matrices.ErrorMatrices("Esta matriz es singular");
            }
            solucion.set(i,0,0.);
            for (int j = 0; j < i; j++) {
                solucion.set(i,0, solucion.get(i,0) + this.get(i,j)*solucion.get(j,0));
            }
            solucion.set(i,0, (b.get(i,0) - solucion.get(i,0))/this.get(i,i));
        }
        return solucion;
    }

    @Override
    public Matriz<Double> traspuesta() {
        final Matriz<Double> this2 = this;
        return new MatrizReal(this.columnas(), this.filas(), new GeneradorMatriz<Double>() {
            @Override
            public Double get(int fila, int columna) {
                return this2.get(columna, fila);
            }
        });
    }

    @Override
    public Matriz<Double> inversa() throws Matrices.ErrorMatrices {
        if (!this.cuadrada()) {
            throw new Matrices.ErrorMatrices("La matriz de solveCramer debe ser cuadrada");
        }
        if (this.determinante() == 0) {
            throw new Matrices.ErrorMatrices("La matriz es singular");
        }

        MatrizReal result = new MatrizReal(this.filas(),this.columnas());

        for (int c = 0; c != this.columnas(); c++){
            // Resolver camer con el sistema Ax = e_c (es decir, la identidad en c).
            final int ej = c;
            Matriz<Double> col = this.solveCramer(new MatrizReal(this.filas(), 1, new GeneradorMatriz<Double>() {
                @Override
                public Double get(int fila, int columna) {
                    return (fila == ej ? 1. : 0.);
                }
            }));
            for (int f = 0; f != this.filas(); f++) {
                result.set(f,c,col.get(f,0));
            }
        }

        return result;
    }

    @Override
    public boolean igual(Matriz<Double> b, double eps) {
        try {
            return this.resta(b).norma1() < eps;
        } catch (Matrices.ErrorMatrices errorMatrices) {
            return false;
        }
    }

    @Override
    public Matriz<Double>[] LUdoolittle() throws Matrices.ErrorMatrices {
        if (!this.cuadrada())
            throw new Matrices.ErrorMatrices("Esta matriz no es cuadrada");

        Matriz<Double>[] result = new Matriz[2];
        Matriz<Double> L = result[0] = new MatrizReal(this.filas(), this.columnas(), new GeneradorMatriz<Double>() {
            @Override
            public Double get(int fila, int columna) {
                return 0.;
            }
        });
        Matriz<Double> U = result[1] = new MatrizReal(this.filas(), this.columnas(), new GeneradorMatriz<Double>() {
            @Override
            public Double get(int fila, int columna) {
                return 0.;
            }
        });

        for (int k = 0; k < this.filas(); k++) {
            double aux = this.get(k,k);
            for (int s = 0; s < k; s++) {
                aux -= L.get(k,s)*U.get(s,k);
            }
            if (Math.abs(aux) < eps) {
                throw new Matrices.ErrorMatrices("Esta matriz es singular");
            }
            L.set(k,k,1.);
            U.set(k,k,aux);

            for (int j = k + 1; j < this.filas(); j++) {
                aux = this.get(k,j);
                for (int s = 0; s < k; s++) {
                    aux -= L.get(k,s) * U.get(s,j);
                }
                U.set(k,j,aux);
            }
            for (int i = k + 1; i < this.filas(); i++) {
                aux = this.get(i,k);
                for (int s = 0; s < k; s++) {
                    aux -= L.get(i,s) * U.get(s,k);
                }
                L.set(i, k, aux / U.get(k, k));
            }
        }
        return result;
    }

    @Override
    public Double determinanteLU() throws Matrices.ErrorMatrices {
        if (!this.cuadrada())
            throw new Matrices.ErrorMatrices("Matriz no cuadrada");
        double result = 1.;
        for (int i = 0; i < this.filas(); i++) {
            result *= this.get(i,i);
        }
        return result;
    }

    @Override
    public Matriz<Double> factorLdeCholeski() throws Matrices.ErrorMatrices {
        if (!this.simetrica()) {
            throw new Matrices.ErrorMatrices("La matriz debe ser simétrica");
        }
        if (!this.cuadrada()) {
            throw new Matrices.ErrorMatrices("La matriz debe ser cuadrada");
        }
        Matriz<Double> result = new MatrizReal(this.filas(), this.columnas(), new GeneradorMatriz<Double>() {
            @Override
            public Double get(int fila, int columna) {
                return 0.;
            }
        });
        for (int k = 0; k < this.filas(); k++) {
            double aux = this.get(k,k);
            for (int s = 0; s < k; s++) {
                aux -= result.get(k,s)*result.get(k,s);
            }
            if (aux <= eps) {
                throw new Matrices.ErrorMatrices("La matriz no admite factorización L de Choleski");
            }
            result.set(k,k, Math.sqrt(aux));
            for (int i = k+1; i<this.filas(); i++) {
                aux = this.get(i,k);
                for (int s = 0; s < k; s++) {
                    aux -= result.get(i,s)*result.get(k, s);
                }
                result.set(i,k,aux/result.get(k,k));
            }
        }
        return result;
    }

    @Override
    public Matriz<Double> solveCholeski(Matriz<Double> b) throws Matrices.ErrorMatrices {
        Matriz<Double> L = this.factorLdeCholeski();
        Matriz<Double> U = L.traspuesta();
        Matriz y = U.ascendente(b);
        return L.descendente(y);
    }

    @Override
    public Matriz<Double> solveGaussP(Matriz<Double> b) throws Matrices.ErrorMatrices {
        if (!this.cuadrada()) {
            throw new Matrices.ErrorMatrices("Matriz no cuadrada");
        }
        if (this.filas() != b.filas()) {
            throw new Matrices.ErrorMatrices("Tamaños incompatibles");
        }
        if (b.columnas() > 1) {
            throw new Matrices.ErrorMatrices("La matriz soluciones debe ser columna");
        }
        Matriz<Double> solucion = new MatrizReal(this.filas(), 1);
        Matriz<Double> B = null;
        Matriz<Double> v = null;
        try {
            B = this.clone();
            v = b.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }

        int[] fila = new int[this.filas()];

        for (int i = 0; i < this.filas(); i++) {
            fila[i] = i;
        }

        for (int k = 0; k < this.columnas() - 1; k++) {
            //Elección del pivote
            int p = k;
            for (int i = k + 1; i < this.filas(); i++) {
                if (Math.abs(B.get(fila[i],k)) > Math.abs(B.get(fila[p],k))) {
                    p = i;
                }
            }
            if (Math.abs(B.get(fila[p],k)) < eps) {
                throw new Matrices.ErrorMatrices("Matriz singular");
            }
            //intercambio de filas
            int m = fila[k];
            fila[k] = fila[p];
            fila[p] = m;
            //eliminación
            for (int i = k + 1; i < this.filas(); i++) {
                double mul = B.get(fila[i],k) / B.get(fila[k],k);
                B.set(fila[i], k, 0.);
                for (int j = k + 1; j < this.columnas(); j++) {
                    B.set(fila[i],j,B.get(fila[i],j) - mul * B.get(fila[k],j));
                }
                v.set(fila[i],0,v.get(fila[i],0)-mul*v.get(fila[k],0));
            }
        }

        //resolvemos por el método ascendente
        for (int k = this.filas() - 1; k >= 0; k--) {
            solucion.set(k,0,v.get(fila[k],0));
            for (int j = k + 1; j < this.columnas(); j++) {
                solucion.set(k,0,solucion.get(k,0) - B.get(fila[k],j) * solucion.get(j,0));

            }
            solucion.set(k,0,solucion.get(k,0) / B.get(fila[k],k));
        }
        return solucion;
    }

    @Override
    public Matriz<Double> clone() throws CloneNotSupportedException {
        final MatrizReal this2 = this;
        return new MatrizReal(this.filas(), this.columnas(), new GeneradorMatriz<Double>() {
            @Override
            public Double get(int fila, int columna) {
                return this2.get(fila,columna);
            }
        });
    }
}
