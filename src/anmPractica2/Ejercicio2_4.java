package anmPractica2;

import auxiliar.Matrices;
import ezequiel.GeneradorMatriz;
import ezequiel.Matriz;
import ezequiel.MatrizReal;

import java.util.Random;

/**
 * Created by lilezek on 13/04/15.
 */
public class Ejercicio2_4 {

    public static Matriz solLU(Matriz L, Matriz U, Matriz b) throws Matrices.ErrorMatrices {
        Matriz y = U.ascendente(b);
        return L.descendente(y);
    }

    public static void main(String[] args) {
        try {
            // Matriz aleatoria:
            Matriz aleatoria = new MatrizReal(3, 3, new GeneradorMatriz<Double>() {
                @Override
                public Double get(int fila, int columna) {
                    return Math.ceil(Math.random()*10.);
                }
            });
            // Probar la factorización doolitle
            Matriz LU[] = aleatoria.LUdoolittle();
            System.out.println(aleatoria);
            System.out.println(LU[0]);
            System.out.println(LU[1]);
            System.out.println(LU[0].producto(LU[1]));

            // Matriz aleatoria:
            aleatoria = new MatrizReal(3, 3, new GeneradorMatriz<Double>() {
                @Override
                public Double get(int fila, int columna) {
                    if (fila == columna)
                        return Math.ceil(Math.random()*10.);
                    else
                        return 0.;
                }
            });

            Matriz b_n = new MatrizReal(3, 1, new GeneradorMatriz<Double>() {
                @Override
                public Double get(int fila, int columna) {
                    return fila+1.;
                }
            });

            LU = aleatoria.LUdoolittle();
            System.out.println(aleatoria);
            System.out.println(b_n);
            System.out.println(solLU(LU[0], LU[1], b_n));

        } catch (Matrices.ErrorMatrices errorMatrices) {
            errorMatrices.printStackTrace();
        }
    }
}
