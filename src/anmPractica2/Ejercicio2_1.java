package anmPractica2;

import auxiliar.Matrices;
import ezequiel.Matriz;
import ezequiel.MatrizReal;

/**
 * Created by lilezek on 12/04/15.
 */
public class Ejercicio2_1 {
    public static double aproxRadioEspectral(Matriz m) throws Matrices.ErrorMatrices {
        Matriz b = m;
        int iteraciones = 100;
        for (int i = 1; i < iteraciones; i++)
            b = b.producto(m);
        return Math.pow(b.norma1(), 1. / (double) iteraciones);
    }

    public static void main(String[] args) {
        try {
            Matriz A = new MatrizReal(4, 4,
                    10., 7., 8., 7.,
                    7., 5., 6., 5.,
                    8., 6., 10., 9.,
                    7., 5., 9., 10.);

            System.out.println("Norma 2 de A: "+aproxRadioEspectral(A)*aproxRadioEspectral(A.inversa()));

        } catch (Matrices.ErrorMatrices errorMatrices) {
            errorMatrices.printStackTrace();
        }
    }
}
