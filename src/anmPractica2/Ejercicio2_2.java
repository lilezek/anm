package anmPractica2;

import auxiliar.Matrices;
import ezequiel.GeneradorMatriz;
import ezequiel.Matriz;
import ezequiel.MatrizReal;

/**
 * Created by lilezek on 12/04/15.
 */
public class Ejercicio2_2 {
    public static void main(String[] args) {
        try {
            Matriz A = new MatrizReal(5,5,
                1., 2.,  1., 3., 2.  ,
                0., 2.,  1., 3., 1.  ,
                0., 0., -1., 4., 0.  ,
                0., 0.,  0., 6., 0.  ,
                0., 0.,  0., 0., 100.);

            Matriz B = new MatrizReal(10, 10, new GeneradorMatriz<Double>() {
                @Override
                public Double get(int fila, int columna) {
                    if (fila <= columna)
                        // (i+j)^2 es como (fila + columna + 2)^2, ya que fila y columna empiezan en 0, no en 1
                        return Math.pow((double) fila + (double) columna + 2, 2);
                    else
                        return 0.;
                }
            });

            Matriz b5 = new MatrizReal(5, 1, new GeneradorMatriz<Double>() {
                @Override
                public Double get(int fila, int columna) {
                    return (double)fila+1;
                }
            });

            Matriz b10 = new MatrizReal(10, 1, new GeneradorMatriz<Double>() {
                @Override
                public Double get(int fila, int columna) {
                    return (double)fila+1;
                }
            });

            // Las soluciones:
            System.out.println(A.ascendente(b5));
            System.out.println(B.ascendente(b10));

        } catch (Matrices.ErrorMatrices errorMatrices) {
            errorMatrices.printStackTrace();
        }
    }
}
